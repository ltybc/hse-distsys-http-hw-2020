import os

import requests
import json
from random import randint

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)


def test_add_ok():
    resp_ok_text = {
        'title': 'Best book',
        'description': 'Best book description'
    }
    response_ok = requests.post(URL + "/add", data=resp_ok_text)
    assert len(str(response_ok.text)) == 10 and int(response_ok.text)
    assert response_ok.status_code == 200


def test_add_not_ok():
    resp_bad_text = {
        'bad': 'request'
    }
    response_not_ok = requests.post(URL + "/add", data=resp_bad_text)
    assert response_not_ok.text == "Invalid input data"
    assert response_not_ok.status_code == 400


def test_get_one():
    unique_id = randint(1, 1000)
    resp_ok_text = {
        'title': 'Best book' + str(unique_id),
        'description': 'Best book description'
    }
    book_id = requests.post(URL + "/add", data=resp_ok_text).text
    response = requests.get(URL + "/", params={'id': book_id})
    assert json.loads(response.text)['title'] == 'Best book' + str(unique_id)
    assert response.status_code == 200


def test_get_all():
    response = requests.get(URL + "/")
    assert response.status_code == 200


def test_get_not_found():
    response = requests.get(URL + "/", params={'id': 'some_sh*t'})
    assert response.status_code == 404


def test_update_ok():
    resp_ok_text = {
        'title': 'Best book',
        'description': 'Old book description'
    }
    book_id = requests.post(URL + "/add", data=resp_ok_text).text
    resp_new = {
        'id': book_id,
        'title': 'New book',
        'description': 'Best book description'
    }
    response_new = requests.put(URL + "/update", data=resp_new)
    assert response_new.status_code == 200
    assert response_new.text == book_id

    response_check = requests.get(URL + "/", params={'id': book_id})
    assert json.loads(response_check.text)['title'] == 'New book'
    assert response_check.status_code == 200


def test_update_new():
    resp_brand_new = {
        'title': 'Brand new book',
        'description': 'Best book description'
    }
    response_create = requests.put(URL + "/update", data=resp_brand_new)
    book_id = response_create.text
    assert response_create.status_code == 200

    response_check = requests.get(URL + "/", params={'id': book_id})
    assert json.loads(response_check.text)['title'] == 'Brand new book'
    assert json.loads(response_check.text)['id'] == book_id
    assert response_check.status_code == 200


def test_delete():
    resp_text = {
        'title': 'Best book',
        'description': 'Best book description'
    }
    response_add = requests.post(URL + "/add", data=resp_text)
    book_id = response_add.text

    response_delete = requests.delete(URL + "/delete", data={'id': book_id})
    assert response_delete.status_code == 200

    response_all = json.loads(requests.get(URL + '/').text)
    assert book_id not in response_all


