import os
import json

from random import randint
from flask import Flask, render_template, request, jsonify

app = Flask(__name__)
database = {}


@app.route('/add', methods=['POST'])
def add_book():
    form = request.form
    title = form.get('title')
    description = form.get('description')
    return add_new_book(description, title)


@app.route('/', methods=['GET'])
def get_books():
    id = request.args.get('id')
    if id is None:
        return jsonify(database), 200
    if database.__contains__(id):
        return jsonify(database[id]), 200
    else:
        return '', 404


@app.route('/update', methods=['PUT'])
def put_books():
    form = request.form
    id = form.get('id')
    title = form.get('title')
    description = form.get('description')
    if id is None:
        return add_new_book(description, title)

    book = database[id]
    if title is not None:
        book['title'] = title
    if description is not None:
        book['description'] = description
    database[id] = book
    return id, 200


@app.route('/delete', methods=['DELETE'])
def delete_book():
    form = request.form
    id = form.get('id')
    if id is None:
        return '', 400
    database.pop(id, None)
    return '', 200


def add_new_book(description, title):
    if title and description:
        id = generate_id()
        book = {'id': id, 'title': title, 'description': description}
        database[id] = book
        return id, 200
    return "Invalid input data", 400


def generate_id():
    return ''.join(["{}".format(randint(0, 9)) for num in range(0, 10)])


app.run(host='0.0.0.0', port=int(os.environ.get('HSE_HTTP_FLASK_PORT', 80)))
